#include "image.h"
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header ;

/*  deserializer   */
enum read_status {
    READ_ERROR,
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PIXEL_LINE,
    FAILED_TO_SKIP_TRASH,
    IMAGE_NOT_FOUND
};

enum read_status from_bmp(FILE *in, struct image *img);

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    OUTPUT_NOT_FOUND
};

enum write_status to_bmp(FILE *out, struct image const *img);
