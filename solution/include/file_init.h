#include <stdio.h>

// Функция для открытия файла на чтение
FILE* open_file_read(const char* filename);

// Функция для открытия файла на запись
FILE* open_file_write(const char* filename);
