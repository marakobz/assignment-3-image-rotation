#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    int32_t width, height;
    struct pixel *data;
};

struct image image_create(int32_t width, int32_t height);

void free_mem(struct image *image);

#endif
