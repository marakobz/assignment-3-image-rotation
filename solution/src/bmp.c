#include "bmp.h"
#include "error_printer.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>

const int16_t BFtype = 0x4D42;
const int16_t BFBITCount = 24;
const int32_t BFReserved = 0;
const int32_t BOffBits = 54;
const int32_t BISize = 40;
const int16_t BIPlanes = 1;
const int32_t BICompression = 0;
const int32_t BIXPelsPerMetr = 2834;
const int32_t BIYPelsPerMetr = 2834;
const int32_t BIClrUsed = 0;
const int32_t BIClrImportant = 0;


static int get_padding(int32_t width) {
    int32_t bytes =(int32_t) sizeof(struct pixel) * width;
    return (4 - bytes % 4) % 4;
}

struct bmp_header {
    int16_t bfType;
    int32_t bfileSize;
    int32_t bfReserved;
    int32_t bOffBits;
    int32_t biSize;
    int32_t biWidth;
    int32_t biHeight;
    int16_t biPlanes;
    int16_t biBitCount;
    int32_t biCompression;
    int32_t biSizeImage;
    int32_t biXPelsPerMeter;
    int32_t biYPelsPerMeter;
    int32_t biClrUsed;
    int32_t biClrImportant;
};

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header bmp ;

    int result_read = (int)fread(&bmp, sizeof(struct bmp_header), 1, in);

    if (!result_read) {
        free_mem(img);
        return READ_INVALID_HEADER;
    }
    if (bmp.bfType != BFtype) {
        free_mem(img);
        return READ_INVALID_SIGNATURE;
    }
    if (bmp.biBitCount != BFBITCount) {
        free_mem(img);
        return READ_INVALID_BITS;
    }


    *img = image_create(bmp.biWidth, bmp.biHeight);
    if (in == NULL){
        free_mem(img);
        return IMAGE_NOT_FOUND;
    }

   if (img->data == NULL) {
        free_mem(img);
        return READ_INVALID_HEADER;
    }

    fseek(in, BOffBits, SEEK_SET);

    int padding = get_padding(img->width);
    for (int32_t i = 0; i < bmp.biHeight; i++) {

        int return_value =  (int) fread(img->data + i * bmp.biWidth, sizeof(struct pixel), bmp.biWidth, in);

        if (return_value != bmp.biWidth) {
            free_mem(img);
            return READ_ERROR;
        }

        if (fseek(in, (long) padding, SEEK_CUR) != 0) {
            free_mem(img);
            return FAILED_TO_SKIP_TRASH;
        }
    }

    return READ_OK;
}

static int32_t get_image_size(int32_t width, int32_t height) {
    int padding = get_padding(width);
    return (int32_t)(sizeof(struct pixel) * width + padding) * height;
}


enum write_status to_bmp(FILE *out, struct image const *img) {
    int padding = get_padding(img->width);
    int32_t imageSize = get_image_size(img->width, img->height);
    int32_t fileSize = (int32_t) (imageSize + sizeof(struct bmp_header));
    struct bmp_header bmp_header = {
        .bfType = BFtype,
        .biSizeImage = imageSize,
        .bfileSize = fileSize,
        .bfReserved = BFReserved,
        .bOffBits = BOffBits,
        .biSize = BISize,
        .biWidth = (int32_t) img->width,
        .biHeight = (int32_t) img->height,
        .biPlanes = BIPlanes,
        .biBitCount = BFBITCount,
        .biCompression = BICompression,
        .biXPelsPerMeter = BIXPelsPerMetr,
        .biYPelsPerMeter = BIYPelsPerMetr,
        .biClrUsed = BIClrUsed,
        .biClrImportant = BIClrImportant,
    };


    int header_write_result = (int) fwrite(&bmp_header, sizeof(bmp_header), 1, out);
        if (header_write_result < 1) {
            return WRITE_ERROR;
    }

    for (int32_t i = 0; i < img->height; i++) {
        int data_write_result = (int) fwrite(img->data + i * (img->width), 3, img->width, out);
        if (data_write_result != img->width) {
            return WRITE_ERROR;
        }

        fseek(out, padding, SEEK_CUR);

    }
    return WRITE_OK;
}

