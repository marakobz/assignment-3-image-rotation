#include "error_printer.h"

#include <stdio.h>

#define READ_ERROR 1
#define READ_INVALID_SIGNATURE 2
#define READ_INVALID_HEADER 3 
#define READ_INVALID_BITS 4
#define READ_INVALID_PIXEL_LINE 5
#define FAILED_TO_SKIP_TRASH 6
#define WRITE_ERROR 7
#define IMAGE_NOT_FOUND 8
#define OUTPUT_NOT_FOUND 9

void print_read_error(int error_code) {
  switch (error_code) {
  case READ_ERROR:
    printf("Mistake has occured while reading BMP file.\n");
    break;
  case READ_INVALID_SIGNATURE:
    printf("Invalid signatue of file.\n");
    break;
  case READ_INVALID_HEADER:
    printf("Invalid header of BMP file.\n");
    break;
  case READ_INVALID_BITS:
    printf("Invalid bits has been read.\n");
    break;
  case READ_INVALID_PIXEL_LINE:
    printf("Invalid pixel line has been read.\n");
    break;
  case FAILED_TO_SKIP_TRASH:
    printf("Failed to skip trash.\n");
    break;
  case IMAGE_NOT_FOUND:
    printf("Cannot find image.\n");
    break;
  default:
    break;
  }
}

void print_write_error(int error_code) {
  switch (error_code) {
  case WRITE_ERROR:
    printf("Mistake occured while writing to BMP file.\n");
    break;
  case OUTPUT_NOT_FOUND:
    printf("Cannot find output file.\n");
    break;
  default:
    break;
  }
}
