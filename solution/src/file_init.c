#include "file_init.h"

FILE* open_file_read(const char* filename) {
    FILE* file = fopen(filename, "rb");
    if (file == NULL)  {
        perror("Mistake occured while openening file to read");
    }
    return file;
}

FILE* open_file_write(const char* filename) {
    FILE* file = fopen(filename, "wb");
    if (file == NULL) {
        perror("Mistake occured while opening the write file");
        
    }
    return file;
}
