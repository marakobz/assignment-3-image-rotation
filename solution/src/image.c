#include "image.h"
#include <stdint.h>
#include <stdlib.h>

struct image image_create(int32_t width, int32_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)
    };
}

void free_mem(struct image *image) {
    if (image->data == NULL) {
        image->width = 0;
        image->height = 0;
    }
    free(image->data);
}
