#include "bmp.h"
#include "error_printer.h"
#include "file_init.h"
#include "image.h"

#include "rotation.h"


#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define INPUT_FILE 1
#define OUTPUT_FILE 2
#define ANGLE 3

#define SUCCESS 0
#define ERROR_EXIT 1

int main(int argc, char* argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Check entered arguments\n");
        return ERROR_EXIT;
    }

    const char* input_file = argv[INPUT_FILE];
    const char* output_file = argv[OUTPUT_FILE];
    int angle = atoi(argv[ANGLE]);

    FILE *in = open_file_read(input_file);

    struct image image;
    int read_code = from_bmp(in, &image);
    print_read_error(read_code);
    if (read_code) return read_code;

    fclose(in);
    image = change_angle(image, angle);

    FILE *out = open_file_write(output_file);
    int write_code = to_bmp(out, &image);
    print_write_error(write_code);
    if (write_code) return write_code;  

    free_mem(&image);
    fclose(out);
    return SUCCESS;
}
