#include "rotation.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


static struct image rotate(struct image const source) {
    struct image image = image_create(source.height, source.width);
    for (int32_t i = 0; i < source.height; i++) {
        for (int32_t j = 0; j < source.width; j++) {
            image.data[source.height * (j + 1) - i - 1] = source.data[source.width * i + j];
        }
    }
    return image;
}

static int32_t calc_rot_number(int32_t angle) {
    return (4 - angle / 90) % 4;
}

struct image change_angle(struct image source, int angle) {
    int32_t rotation_number = calc_rot_number(angle);
    for (int32_t i = 0; i < rotation_number; i++) {
        struct image rotated = rotate(source);
        free_mem(&source);
        source = rotated;

    }
    return source;
}


